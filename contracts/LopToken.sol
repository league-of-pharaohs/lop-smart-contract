/**
 *Submitted for verification at BscScan.com on 2021-11-20
*/

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IERC20 {
    function totalSupply() external view returns (uint);

    function balanceOf(address account) external view returns (uint);

    function transfer(address recipient, uint amount)
    external
    returns (bool);

    function allowance(address owner, address spender)
    external
    view
    returns (uint);

    function approve(address spender, uint amount) external returns (bool);

    function transferFrom(
        address sender,
        address recipient,
        uint amount
    ) external returns (bool);

    event Transfer(address indexed from, address indexed to, uint value);
    event Approval(
        address indexed owner,
        address indexed spender,
        uint value
    );
}

contract LopToken is IERC20 {
    uint private constant eighteen_decimals_value = 1_000_000_000_000_000_000;
    // ERC20 variables
    mapping(address => uint) private _balances;
    mapping(address => mapping(address => uint)) private _allowances;
    uint private _totalSupply = 10_000_000_000 * eighteen_decimals_value;

    // General variables
    string public constant name = "League Of Pharaohs";
    string public constant symbol = "LOP";
    uint8 public constant decimals = 18;
    address public _admin;

    // External contract general variables
    uint _preSaleAmountCap = 200_000_000 * eighteen_decimals_value;
    address public _preSaleContract;
    bool public _hasPreSaleContractNotYetSet = true;

    // Utility variables
    bool public _isPaused;
    mapping(address => bool) public _isPausedAddress;

    // Daily date variable
    uint public constant _NOV_20_2022 = 1_668_902_400; // first day

    // Monthly date variables
    uint public constant _Dec_20_2022 = 1_671_494_400;

    uint public constant _Jan_20_2023 = 1_674_172_800;
    uint public constant _Feb_20_2023 = 1_676_851_200;
    uint public constant _Mar_20_2023 = 1_679_270_400;
    uint public constant _Apr_20_2023 = 1_681_948_800;
    uint public constant _May_20_2023 = 1_684_540_800;
    uint public constant _Jun_20_2023 = 1_687_219_200;
    uint public constant _Jul_20_2023 = 1_689_811_200;
    uint public constant _Aug_20_2023 = 1_692_489_600;
    uint public constant _Sep_20_2023 = 1_695_168_000;
    uint public constant _Oct_20_2023 = 1_697_760_000;
    uint public constant _Nov_20_2023 = 1_700_438_400;
    uint public constant _Dec_20_2023 = 1_703_030_400;

    uint public constant _Jan_20_2024 = 1_705_708_800;
    uint public constant _Feb_20_2024 = 1_708_387_200;
    uint public constant _Mar_20_2024 = 1_710_892_800;
    uint public constant _Apr_20_2024 = 1_713_571_200;
    uint public constant _May_20_2024 = 1_716_163_200;
    uint public constant _Jun_20_2024 = 1_718_841_600;
    uint public constant _Jul_20_2024 = 1_721_433_600;
    uint public constant _Aug_20_2024 = 1_724_112_000;
    uint public constant _Sep_20_2024 = 1_726_790_400;
    uint public constant _Oct_20_2024 = 1_729_382_400;
    uint public constant _Nov_20_2024 = 1_732_060_800;
    uint public constant _Dec_20_2024 = 1_734_652_800;

    uint public constant _Jan_20_2025 = 1_737_331_200;
    uint public constant _Feb_20_2025 = 1_740_009_600;
    uint public constant _Mar_20_2025 = 1_742_428_800;
    uint public constant _Apr_20_2025 = 1_745_107_200;
    uint public constant _May_20_2025 = 1_747_699_200;
    uint public constant _Jun_20_2025 = 1_750_377_600;
    uint public constant _Jul_20_2025 = 1_752_969_600;
    uint public constant _Aug_20_2025 = 1_755_648_000;
    uint public constant _Sep_20_2025 = 1_758_326_400;
    uint public constant _Oct_20_2025 = 1_760_918_400;
    uint public constant _Nov_20_2025 = 1_763_596_800;
    uint public constant _Dec_20_2025 = 1_766_188_800;

    uint public constant _Jan_20_2026 = 1_768_867_200;
    uint public constant _Feb_20_2026 = 1_771_545_600;
    uint public constant _Mar_20_2026 = 1_773_964_800;


    uint[] public _monthlyDates; // The cutoff monthly dates that allow coin distribution
    mapping(string => uint) public _categoriesAmountCap; // The maximum amount allowed to be transfer (Category => Cap)
    mapping(string => address) public _categoriesAddress; // Address for categories (Category => Address)
    //mapping(string => mapping(uint => uint)) public _dailyCoinDistribution; // Daily coin distribution schedule (Category => Unix Date => Amount)
    mapping(string => mapping(uint => uint)) public _monthlyCoinDistribution; // Monthly coin distribution schedule (Category => Unix Date => Amount)

    uint public _dailyIndex;
    mapping(string => uint) public _dailyCategoryIndex;

    event OutOfMoney(string category);  // emit when `_categoriesAmountCap` less than required amount.

    constructor() {
        _admin = msg.sender;
        _balances[address(this)] = _totalSupply;

        // Add all addresses
        _categoriesAddress['Advisors1'] = 0x78f7ec8a3baF52f5e2c821d784219e68Eb95331c;
        _categoriesAddress['Advisors2'] = 0x1Bd4F4118e84183e8935EB734d566493D2B14a70;
        _categoriesAddress['Advisors3'] = 0xEB705Cb0Ba6aa4ec94Eb3b1B6BdA7D0021F56b9C;
        _categoriesAddress['Advisors4'] = 0x9e65f7c5afa74259eB7ec107729F1B8DD86cFE3d;
        _categoriesAddress['Team'] = 0xc802117004EFbc80Add239e9fb75f2c5Dde9d03e;
        _categoriesAddress['Marketing'] = 0x1Edcd4dA7074739603D4a38e7dB6056a272da464;
        _categoriesAddress['EcosystemFund'] = 0x33565a6615a520f2F03A3D8085a1bC4c2f353333;
        _categoriesAddress['PlayToEarn'] = 0xF5cE33028F7C0DF0ac6F6026705c374477Ff3ebB;
        _categoriesAddress['Staking'] = 0xFd112Ab263F5494C632005C0CC22B45b86686b13;
        _categoriesAddress['NFTStaking'] = 0xa63a4ee51B5E126beAD9c73D2c021b9fe58087A2;
        _setDefaultValues();
        _initialTransfer();
        _setMonthlyDistribution();
        //_setMonthlyDistributionTesting();
    }

    //    constructor(
    //        address advisors1Address,
    //        address advisors2Address,
    //        address advisors3Address,
    //        address advisors4Address,
    //        address teamAddress,
    //        address marketingAddress,
    //        address ecosystemFundAddress,
    //        address playToEarnAddress,
    //        address stakingAddress,
    //        address nftStakingAddress
    //    ) {
    //        _admin = msg.sender;
    //        _balances[address(this)] = _totalSupply;
    //
    //        // Add all addresses
    //        _categoriesAddress['Advisors1'] = advisors1Address;
    //        _categoriesAddress['Advisors2'] = advisors2Address;
    //        _categoriesAddress['Advisors3'] = advisors3Address;
    //        _categoriesAddress['Advisors4'] = advisors4Address;
    //        _categoriesAddress['Team'] = teamAddress;
    //        _categoriesAddress['Marketing'] = marketingAddress;
    //        _categoriesAddress['EcosystemFund'] = ecosystemFundAddress;
    //        _categoriesAddress['PlayToEarn'] = playToEarnAddress;
    //        _categoriesAddress['Staking'] = stakingAddress;
    //        _categoriesAddress['NFTStaking'] = nftStakingAddress;
    //        _setDefaultValues();
    //        _initialTransfer();
    //        //_setMonthlyDistribution();
    //        _setMonthlyDistributionTesting();
    //    }

    function _setDefaultValues() private {
        // Add all monthly dates

        _monthlyDates.push(_Nov_20_2022);
        _monthlyDates.push(_Dec_20_2022);
        _monthlyDates.push(_Jan_20_2023);
        _monthlyDates.push(_Feb_20_2023);
        _monthlyDates.push(_Mar_20_2023);
        _monthlyDates.push(_Apr_20_2023);
        _monthlyDates.push(_May_20_2023);
        _monthlyDates.push(_Jun_20_2023);
        _monthlyDates.push(_Jul_20_2023);
        _monthlyDates.push(_Aug_20_2023);
        _monthlyDates.push(_Sep_20_2023);
        _monthlyDates.push(_Oct_20_2023);
        _monthlyDates.push(_Nov_20_2023);
        _monthlyDates.push(_Dec_20_2023);
        _monthlyDates.push(_Jan_20_2024);
        _monthlyDates.push(_Feb_20_2024);
        _monthlyDates.push(_Mar_20_2024);
        _monthlyDates.push(_Apr_20_2024);
        _monthlyDates.push(_May_20_2024);
        _monthlyDates.push(_Jun_20_2024);
        _monthlyDates.push(_Jul_20_2024);
        _monthlyDates.push(_Aug_20_2024);
        _monthlyDates.push(_Sep_20_2024);
        _monthlyDates.push(_Oct_20_2024);
        _monthlyDates.push(_Nov_20_2024);
        _monthlyDates.push(_Dec_20_2024);
        _monthlyDates.push(_Jan_20_2025);
        _monthlyDates.push(_Feb_20_2025);
        _monthlyDates.push(_Mar_20_2025);
        _monthlyDates.push(_Apr_20_2025);
        _monthlyDates.push(_May_20_2025);
        _monthlyDates.push(_Jun_20_2025);
        _monthlyDates.push(_Jul_20_2025);
        _monthlyDates.push(_Aug_20_2025);
        _monthlyDates.push(_Sep_20_2025);
        _monthlyDates.push(_Oct_20_2025);
        _monthlyDates.push(_Nov_20_2025);
        _monthlyDates.push(_Dec_20_2025);
        _monthlyDates.push(_Jan_20_2026);
        _monthlyDates.push(_Feb_20_2026);
        _monthlyDates.push(_Mar_20_2026);


        // Add all amount cap
        _categoriesAmountCap['Advisors1'] = 100_000_000 * eighteen_decimals_value;
        _categoriesAmountCap['Advisors2'] = 100_000_000 * eighteen_decimals_value;
        _categoriesAmountCap['Advisors3'] = 100_000_000 * eighteen_decimals_value;
        _categoriesAmountCap['Advisors4'] = 100_000_000 * eighteen_decimals_value;
        _categoriesAmountCap['Team'] = 2_000_000_000 * eighteen_decimals_value;
        _categoriesAmountCap['Marketing'] = 500_000_000 * eighteen_decimals_value;
        _categoriesAmountCap['EcosystemFund'] = 400_000_000 * eighteen_decimals_value;
        _categoriesAmountCap['NFTStaking'] = 800_000_000 * eighteen_decimals_value;
        _categoriesAmountCap['Staking'] = 1_500_000_000 * eighteen_decimals_value;
        _categoriesAmountCap['PlayToEarn'] = 2_500_000_000 * eighteen_decimals_value;
    }
    /*
        function setDailyDistribution(uint number) external onlyAdmin {
            uint i = _dailyIndex;
            uint last = i + number;
            if(last > 1827) last = 1827;

            for (; i < last; i++) {
                uint date = _DEC_12_2021 + 86400 * i;

                if(i < 1826){
                    _dailyCoinDistribution['Advisors1'][date] = 5_476 * eighteen_decimals_value;
                    _dailyCoinDistribution['Advisors2'][date] = 5_476 * eighteen_decimals_value;
                    _dailyCoinDistribution['Advisors3'][date] = 5_476 * eighteen_decimals_value;
                    _dailyCoinDistribution['Advisors4'][date] = 5_476 * eighteen_decimals_value;
                    _dailyCoinDistribution['Team'][date] = 109_529 * eighteen_decimals_value;
                }

                if(i == 1826) {
                    date = _DEC_12_2021 + 86400 * 1826;
                    _dailyCoinDistribution['Advisors1'][date] = 824 * eighteen_decimals_value;
                    _dailyCoinDistribution['Advisors2'][date] = 824 * eighteen_decimals_value;
                    _dailyCoinDistribution['Advisors3'][date] = 824 * eighteen_decimals_value;
                    _dailyCoinDistribution['Advisors4'][date] = 824 * eighteen_decimals_value;
                    _dailyCoinDistribution['Team'][date] = 46;
                }

                if(i < 1095){
                    date = _DEC_12_2021 + 86400 * i;
                    _dailyCoinDistribution['Marketing'][date] = 45_662 * eighteen_decimals_value;
                }

                if(i == 1095){
                    date = _DEC_12_2021 + 86400 * 1095;
                    _dailyCoinDistribution['Marketing'][date] = 110 * eighteen_decimals_value;
                }
            }

            _dailyIndex = i;
        }
    */
    function _setMonthlyDistribution() private {
        _monthlyCoinDistribution['EcosystemFund'][_Dec_20_2022] = 66666666 * eighteen_decimals_value;
        _monthlyCoinDistribution['EcosystemFund'][_Jan_20_2023] = 66666666 * eighteen_decimals_value;
        _monthlyCoinDistribution['EcosystemFund'][_Feb_20_2023] = 66666666 * eighteen_decimals_value;
        _monthlyCoinDistribution['EcosystemFund'][_Mar_20_2023] = 66666666 * eighteen_decimals_value;
        _monthlyCoinDistribution['EcosystemFund'][_Apr_20_2023] = 66666666 * eighteen_decimals_value;
        _monthlyCoinDistribution['EcosystemFund'][_May_20_2023] = 66666670 * eighteen_decimals_value;

        _monthlyCoinDistribution['NFTStaking'][_Feb_20_2023] = 80000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['NFTStaking'][_Mar_20_2023] = 80000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['NFTStaking'][_Apr_20_2023] = 80000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['NFTStaking'][_May_20_2023] = 80000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['NFTStaking'][_Jun_20_2023] = 80000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['NFTStaking'][_Jul_20_2023] = 80000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['NFTStaking'][_Aug_20_2023] = 80000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['NFTStaking'][_Sep_20_2023] = 80000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['NFTStaking'][_Oct_20_2023] = 80000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['NFTStaking'][_Feb_07_2023] = 80000000 * eighteen_decimals_value;

        _monthlyCoinDistribution['Staking'][_Feb_20_2023] = 150000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['Staking'][_Mar_20_2023] = 150000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['Staking'][_Apr_20_2023] = 150000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['Staking'][_May_20_2023] = 150000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['Staking'][_Jun_20_2023] = 150000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['Staking'][_Jul_20_2023] = 150000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['Staking'][_Aug_20_2023] = 150000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['Staking'][_Sep_20_2023] = 150000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['Staking'][_Oct_20_2023] = 150000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['Staking'][_Nov_20_2023] = 150000000 * eighteen_decimals_value;

        _monthlyCoinDistribution['PlayToEarn'][_May_20_2023] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Jun_20_2023] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Jul_20_2023] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Aug_20_2023] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Sep_20_2023] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Oct_20_2023] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Nov_20_2023] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Dec_20_2023] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Jan_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Feb_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Mar_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Apr_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_May_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Jun_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Jul_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Aug_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Sep_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Oct_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Nov_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Dec_20_2024] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Jan_20_2025] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Feb_20_2025] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Mar_20_2025] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_Apr_20_2025] = 100000000 * eighteen_decimals_value;
        _monthlyCoinDistribution['PlayToEarn'][_May_20_2025] = 100000000 * eighteen_decimals_value;
    }

    //    function _setMonthlyDistributionTesting() private {
    //        _monthlyCoinDistribution['EcosystemFund'][_Jul_12_2022]=555555 * eighteen_decimals_value; // Day 213
    //        _monthlyCoinDistribution['EcosystemFund'][_Aug_12_2022]=555555 * eighteen_decimals_value; // Day 244
    //        _monthlyCoinDistribution['EcosystemFund'][_Sep_12_2022]=555555 * eighteen_decimals_value; // Day 275
    //        _monthlyCoinDistribution['EcosystemFund'][_Oct_12_2022]=555555 * eighteen_decimals_value; // Day 305
    //        _monthlyCoinDistribution['EcosystemFund'][_Dec_12_2026]=555585 * eighteen_decimals_value; // Day 367
    //        _monthlyCoinDistribution['NFTStaking'][_Apr_12_2022] = 40000000 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['NFTStaking'][_Apr_12_2023] = 40000000 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['Staking'][_May_12_2022] = 2678571 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['Staking'][_Jun_12_2022] = 2678571 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['Staking'][_Jul_12_2022] = 2678571 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['Staking'][_Aug_12_2022] = 2678571 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['Staking'][_Sep_12_2022] = 2678571 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['PlayToEarn'][_Nov_12_2022] = 5000000 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['PlayToEarn'][_Dec_12_2022] = 5000000 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['PlayToEarn'][_Jan_12_2023] = 5000000 * eighteen_decimals_value;
    //        _monthlyCoinDistribution['PlayToEarn'][_Feb_12_2023] = 5000000 * eighteen_decimals_value;
    //    }
    /**
     * Transfer to 5 addresses when contract are created
     */
    function _initialTransfer() private{
        _transfer(address(this), 0xE0d43d5fdfA1D1eA9Bd3Af1C63c5409e5478068D, 1_000_000_000 * eighteen_decimals_value); // Private Sale
        _transfer(address(this), 0x6A741a293fE0cF3779DcBaD9055F1B0c0B0a7D5A, 100_000_000 * eighteen_decimals_value); // Community
        _transfer(address(this), 0xb992Eec4CAde5f852988C150A3cFbb2DC53e6981, 100_000_000 * eighteen_decimals_value); // Liquidity
        _transfer(address(this), 0xD99477944841B5bf21D28fB7D8F53cA2FFE32d12, 500_000_000 * eighteen_decimals_value); // CEX Reserve
    }

    /**
     * Modifiers
     */
    modifier onlyAdmin() { // Is Admin?
        require(_admin == msg.sender);
        _;
    }

    modifier hasPreSaleContractNotYetSet() { // Has preSale Contract set?
        require(_hasPreSaleContractNotYetSet);
        _;
    }

    modifier isPreSaleContract() { // Is preSale the contract that is currently interact with this contract?
        require(msg.sender == _preSaleContract);
        _;
    }

    modifier whenPaused() { // Is pause?
        require(_isPaused, "Pausable: not paused Erc20");
        _;
    }

    modifier whenNotPaused() { // Is not pause?
        require(!_isPaused, "Pausable: paused Erc20");
        _;
    }

    // Transfer ownernship
    function transferOwnership(address payable admin) external onlyAdmin {
        require(admin != address(0), "Zero address");
        _admin = admin;
    }

    /**
     * Update external contract functions
     */
    function setPreSaleContractNotYetSet(address preSaleContract) external onlyAdmin hasPreSaleContractNotYetSet {
        require(preSaleContract != address(0), "Zero address");
        _preSaleContract = preSaleContract;
        _hasPreSaleContractNotYetSet = false;
    }

    /**
     * ERC20 functions
     */
    function totalSupply() public view override returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account) external override view returns (uint){
        return _balances[account];
    }

    function transfer(address recipient, uint amount) external virtual override returns (bool){
        _transfer(msg.sender, recipient, amount);
        return true;
    }

    function allowance(address owner, address spender) external virtual override view returns (uint){
        return _allowances[owner][spender];
    }

    function approve(address spender, uint amount) external virtual override returns (bool){
        _approve(msg.sender, spender, amount);
        return true;
    }

    function transferFrom(address sender, address recipient, uint amount) external virtual override returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, msg.sender, _allowances[sender][msg.sender]-amount);
        return true;
    }

    /**
     * @dev Atomically increases the allowance granted to `spender` by the caller.
   *
   * This is an alternative to {approve} that can be used as a mitigation for
   * problems described in {ERC20-approve}.
   *
   * Emits an {Approval} event indicating the updated allowance.
   *
   * Requirements:
   *
   * - `spender` cannot be the zero address.
   */
    function increaseAllowance(address spender, uint256 addedValue) public returns (bool) {
        _approve(msg.sender, spender, _allowances[msg.sender][spender] + addedValue);
        return true;
    }

    /**
     * @dev Atomically decreases the allowance granted to `spender` by the caller.
   *
   * This is an alternative to {approve} that can be used as a mitigation for
   * problems described in {ERC20-approve}.
   *
   * Emits an {Approval} event indicating the updated allowance.
   *
   * Requirements:
   *
   * - `spender` cannot be the zero address.
   * - `spender` must have allowance for the caller of at least
   * `subtractedValue`.
   */
    function decreaseAllowance(address spender, uint256 subtractedValue) public returns (bool) {
        _approve(msg.sender, spender, _allowances[msg.sender][spender] - subtractedValue);
        return true;
    }

    function _transfer(address sender, address recipient, uint amount ) internal virtual {
        require(!_isPaused, "ERC20Pausable: token transfer while paused");
        require(!_isPausedAddress[sender], "ERC20Pausable: token transfer while paused on address");
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");
        require(recipient != address(this), "ERC20: transfer to the token contract address");

        _balances[sender] = _balances[sender] - amount;
        _balances[recipient] = _balances[recipient] + amount;
        emit Transfer(sender, recipient, amount);
    }

    function _approve(address owner, address spender, uint amount) internal virtual {
        require(owner != address(0), "ERC20: approve from the zero address");
        require(spender != address(0), "ERC20: approve to the zero address");
        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    /**
     * External contract transfer functions
     * Allow preSale external contract to trigger transfer function
     */

    function transferPresale(address recipient, uint amount) external isPreSaleContract returns (bool) {
        require(_preSaleAmountCap >= amount, 'No more amount allocates for preSale');
        _preSaleAmountCap = _preSaleAmountCap - amount;
        _transfer(address(this), recipient, amount);
        return true;
    }

    /**
     * Daily transfer
     * For advisors1, advisors2, advisors3, advisors4, team and marketing.
     */
    function dailyTransfer() external {
        require(block.timestamp >= _FEB_07_2022, "Too early for daily transfer");
        string memory category = '';
        uint dailyDistribution;

        if(_categoriesAddress['Advisors1'] == msg.sender){
            category = 'Advisors1';
            dailyDistribution = 277_777 * eighteen_decimals_value;
        } else if(_categoriesAddress['Advisors2'] == msg.sender){
            category = 'Advisors2';
            dailyDistribution = 277_777 * eighteen_decimals_value;
        } else if(_categoriesAddress['Advisors3'] == msg.sender){
            category = 'Advisors3';
            dailyDistribution = 277_777 * eighteen_decimals_value;
        } else if(_categoriesAddress['Advisors4'] == msg.sender){
            category = 'Advisors4';
            dailyDistribution = 277_777 * eighteen_decimals_value;
        } else if(_categoriesAddress['Team'] == msg.sender){
            category = 'Team';
            dailyDistribution = 1_388_888 * eighteen_decimals_value;
        } else if(_categoriesAddress['Marketing'] == msg.sender){
            category = 'Marketing';
            dailyDistribution = 833_333 * eighteen_decimals_value;
        }

        require(bytes(category).length > 0, 'Invalid sender address for daily transfer');

        uint amountLeft = _categoriesAmountCap[category];
        require (amountLeft > 0, "All amount was paid");
        uint maxDayToPay = amountLeft / dailyDistribution; // number of days that may be paid with dailyDistribution amount
        uint day = (block.timestamp - _FEB_07_2022) / 86400 + 1; // number of days to pay
        uint unpaidDays = day - _dailyCategoryIndex[category];  // number of days to pay - already paid days
        if (unpaidDays > maxDayToPay + 1) unpaidDays = maxDayToPay + 1;
        uint amount = unpaidDays * dailyDistribution;  // total amount to pay for passed days
        if (amount > amountLeft) amount = amountLeft;   // transfer all tokens that left
        if (amount > 0) {
            bool canTransfer = _categoriesTransfer(msg.sender, amount, category);
            if(canTransfer) {
                _dailyCategoryIndex[category] = day;    // store last paid day
            }
        }
    }

    /**
     * Monthly transfer
     * For EcosystemFund, PlayToEarn, Staking and NftStaking.
     */
    function monthlyTransfer() external {
        string memory category = '';

        if(_categoriesAddress['EcosystemFund'] == msg.sender){
            category = 'EcosystemFund';
        } else if(_categoriesAddress['PlayToEarn'] == msg.sender){
            category = 'PlayToEarn';
        } else if(_categoriesAddress['Staking'] == msg.sender){
            category = 'Staking';
        } else if(_categoriesAddress['NFTStaking'] == msg.sender){
            category = 'NFTStaking';
        }

        require(bytes(category).length > 0, 'Invalid sender address for monthly transfer');

        for (uint y = 0; y < _monthlyDates.length; y++) {
            if(block.timestamp >= _monthlyDates[y]) {
                uint amount = _monthlyCoinDistribution[category][_monthlyDates[y]];
                if(amount > 0){
                    bool canTransfer = _categoriesTransfer(msg.sender, amount, category);

                    if(canTransfer){
                        _monthlyCoinDistribution[category][_monthlyDates[y]]= 0;
                    }
                }
            }
        }
    }

    function _categoriesTransfer(address recipient, uint amount, string memory categories) private returns (bool){
        if (_categoriesAmountCap[categories] < amount) {
            emit OutOfMoney(categories);
            return false;
        }
        _categoriesAmountCap[categories] = _categoriesAmountCap[categories] - amount;
        _transfer(address(this), recipient, amount);
        return true;
    }

    function pause() external onlyAdmin whenNotPaused {
        _isPaused = true;
    }

    function unpause() external onlyAdmin whenPaused {
        _isPaused = false;
    }

    function pausedAddress(address sender) external onlyAdmin {
        _isPausedAddress[sender] = true;
    }

    function unPausedAddress(address sender) external onlyAdmin {
        _isPausedAddress[sender] = false;
    }

    receive() external payable {
        revert();
    }
}
